package controller

import java.util.Date
import scala.annotation.meta.beanGetter
import scala.beans.BeanProperty
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull
import java.util.Formatter.DateTime
import java.util.ArrayList


class Poll (
      @JsonProperty("question") @BeanProperty @(NotNull @beanGetter) var question:String,
      @JsonProperty("expired_at") @BeanProperty @(NotNull @beanGetter) var expired_at:Date,
      @JsonProperty("started_at") @BeanProperty @(NotNull @beanGetter) var started_at:Date,
      @JsonProperty("choice") @BeanProperty @(NotNull @beanGetter) var choice:ArrayList[String]) {
  
      var poll_id:String=null
      var results:ArrayList[Int] = new ArrayList[Int]

      def setId(poll_id : String)={
        this.poll_id = poll_id
      }
      def getId():String={
        return this.poll_id
      }
  
      def setResults(results : ArrayList[Int])={
        this.results = results
      }
      def getResults() : ArrayList[Int]={
        return this.results
      }
}