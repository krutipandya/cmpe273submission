package controller

import java.util.{ArrayList, Date, HashMap, Iterator}
import org.springframework.boot._
import org.springframework.boot.autoconfigure._
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation._
import org.springframework.http.HttpStatus
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid
import scala.collection.JavaConverters._



object Controller {

  def main(args: Array[String]) {
    SpringApplication.run(classOf[Controller])
  }
}

@Controller
@EnableAutoConfiguration
@RequestMapping(value=Array("/api/v1"))
class Controller {

  var modList = new ArrayList[Moderator]()
  var checkUser = new HashMap[String,String]()
  checkUser.put("foo:bar", "authorized")

  
  @RequestMapping(value=Array("/moderators"),method=Array(RequestMethod.POST),headers = Array("Accept=application/json"))
  @ResponseBody
  def createVoteModerator(@RequestBody @Valid mod: Moderator, bindingResult: BindingResult, res: HttpServletResponse, req: HttpServletRequest): Any = {
    val errMsg:String = validate(bindingResult,"Encountered error upon creating moderator : ")
    
    if(errMsg=="") {
      var mod_id: Int = Math.round(Math.random() * (1000000 - 800000 + 5) + 50000).toInt
      mod.setId(mod_id)
      mod.setCreatedAt(new Date())
      modList.add(mod)
      
      res.setStatus(HttpServletResponse.SC_CREATED)
          return "{\"id\" : \"" + mod.mod_id + "\", \"name\" : \"" + mod.name + "\", \"email\" : \"" + mod.email + "\", \"password\" : \"" + mod.password + "\", \"created_at\" : \"" + mod.created_at + "\"}"
    }else{
      res.setStatus(HttpServletResponse.SC_BAD_REQUEST)
      return errMsg
    }
  }

  @RequestMapping(value=Array("/moderators/{mod_id}") ,method=Array(RequestMethod.GET))
  @ResponseBody
  def viewVoteModerator(@PathVariable mod_id:Int, req : HttpServletRequest, res: HttpServletResponse): Any = {
   
    doHttpBasicAuth(req, res);
    val mit = modList.iterator()
    while (mit.hasNext()) {
      val mod: Moderator = mit.next()
      return "{\"id\" : \"" + mod.mod_id + "\", \"name\": \"" + mod.name + "\", \"email\" : \"" + mod.email + "\", \"password\" : \"" + mod.password + "\",\"created_at\" : \"" + mod.created_at + "\"}"
    }
    "Cannot find moderator"
  }

  
  @RequestMapping(value=Array("/moderators/{mod_id}") ,method=Array(RequestMethod.PUT), headers = Array("Accept=application/json"))
  @ResponseBody
  def updateVoteModerator(@PathVariable mod_id:String , @RequestBody mod: Moderator, bindingResult: BindingResult, req : HttpServletRequest, res: HttpServletResponse): Any = {
    
    doHttpBasicAuth(req, res);
    val errMsg:String = validate(bindingResult,"Error while updating moderator : \n")
    if(errMsg=="") {
      val mit = modList.iterator()
      while (mit.hasNext()) {
        val mod: Moderator = mit.next()
        if ((mod.mod_id.toString()).equals(mod_id)) {
          
          mod.setName(mod.name)
          mod.setEmail(mod.email)
          mod.setPassword(mod.password)
          mod.setUpdatedAt(new Date())
         
          return "{\"id\" : \"" + mod.mod_id + "\", \"name\": \""+ mod.name +"\" ,\"email\" : \"" + mod.email + "\", \"password\" : \"" + mod.password + "\", \"created_at\" : \"" + mod.created_at + "\"}"
        }
      }
    }else{
      return errMsg
    }
  }

  @RequestMapping(value=Array("/moderators/{mod_id}/polls") ,method=Array(RequestMethod.POST),headers = Array("Accept=application/json"))
  @ResponseBody
  def createVotePoll(@PathVariable mod_id:String , @RequestBody @Valid p: Poll, bindingResult: BindingResult,req : HttpServletRequest, res: HttpServletResponse): Any = {
    
    doHttpBasicAuth(req, res);
    
    val errMsg:String = validate(bindingResult,"Encountered error upon creating poll ")
    if(errMsg equals "") {
    val iterator = modList.iterator()
    while (iterator.hasNext()) {
      val mod: Moderator = iterator.next()
      
      if ((mod.mod_id.toString()).equals(mod_id)) {
        
         var  random : Int = Math.round(Math.random() * (1000000 - 800000 + 1) + 50000).toInt
          p.poll_id = Integer.toString(random, 36)
          
          val results : ArrayList[Int] = new ArrayList[Int]
          var choices = p.choice
          
          for(a <- 0 to choices.size()-1)
          {
            results.add(a, 0)
          }
          
          p.setResults(results)
          mod.polls.add(p)
          
          return "{ \"id\" : \"" + p.poll_id + "\", \"question\" : \"" + p.question + "\", \"started_at\" : \"" + p.started_at + "\", \"expired_at\" : \"" + p.expired_at + "\", \"choice\" : \"" + p.choice + "\"}"
      }
    }
    }else{
      return errMsg
    }
  }
  
  
  @RequestMapping(value=Array("/polls/{poll_id}") ,method=Array(RequestMethod.GET))
  @ResponseBody
  def viewPollWithoutResult(@PathVariable poll_id:String, req : HttpServletRequest, res: HttpServletResponse): Any = {
    val mit = modList.iterator()
    while (mit.hasNext()) {
      val mod: Moderator = mit.next()
      val pit = mod.getPollList().iterator()
      
      while (pit.hasNext()){
          val p : Poll = pit.next()
          
          if ((p.poll_id).equals(poll_id)) 
          {
            return "{\"id\" : \"" + p.poll_id + "\", \"question\": \"" + p.question + "\", \"started_at\" : \"" + p.started_at + "\", \"expired_at\" : \"" + p.expired_at + "\",\"choice\" : \"" + p.choice + "\"}"
          }
      }
    }
  }
  
  @RequestMapping(value=Array("moderators/{mod_id}/polls/{poll_id}") ,method=Array(RequestMethod.GET))
  @ResponseBody
  def viewPollWithResult(@PathVariable mod_id:Int, @PathVariable poll_id:String, req : HttpServletRequest, res: HttpServletResponse): Any = {
    
    val mit = modList.iterator()
    while (mit.hasNext()) {
      
      val mod: Moderator = mit.next()
      if(mod.mod_id.toString().equals(mod_id)){
          val pit = mod.getPollList().iterator()
          
          while (pit.hasNext()){
          val p: Poll = pit.next()
          if ((p.poll_id).equals(poll_id)) {
          return "{\"id\" : \"" + p.poll_id + "\", \"question\": \"" + p.question + "\", \"started_at\" : \"" + p.started_at + "\", \"expired_at\" : \"" + p.expired_at + "\",\"choice\" : \"" + p.choice + "\", \"results\" : \"" + p.results + "\"}"
        }
      }
      }else{
        return "Cannot find moderator"
      }
    }
  }
  
  
  @RequestMapping(value=Array("moderators/{mod_id}/polls") ,method=Array(RequestMethod.GET))
  @ResponseBody
  def viewAllPolls(@PathVariable mod_id:String, req : HttpServletRequest, res: HttpServletResponse): Any = {
    
    val mit = modList.iterator()
    val pollItems = new StringBuffer()
    while (mit.hasNext()) {
      val mod: Moderator = mit.next()
      
      if(mod.id.toString().equals(mod_id)){
        
          val pit = mod.getpList().iterator()
          
          var flag:Int = 0
          
          while (pit.hasNext()){
            
          val p: Poll = pit.next()
         
          if(flag==0)
          {
            pollItems.append("{\"id\" : \"" + poll.id + "\", \"question\": \"" + poll.question + "\", \"started_at\" : \"" + poll.started_at + "\", \"expired_at\" : \"" + poll.expired_at + "\",\"choice\" : \"" + poll.choice + "\", \"results\" : \"" + poll.results + "\"}")
          }else
          {
            pollItems.append(", {\"id\" : \"" + poll.id + "\", \"question\": \"" + poll.question + "\", \"started_at\" : \"" + poll.started_at + "\", \"expired_at\" : \"" + poll.expired_at + "\",\"choice\" : \"" + poll.choice + "\", \"results\" : \"" + poll.results + "\"}")
          }
          
          flag = flag + 1
      }
       if(flag==0){
         
         return "Cannot find poll"
         
      } else if(flag==1) {
        
        return pollItems.toString()
      } else {
        return "[" + pollItems.toString() + "]"
      }
      }else{
        
        return "Incorrect moderator"
      }
    }
  }
   
  @RequestMapping(value=Array("/moderators/{mod_id}/polls/{poll_id}") ,method=Array(RequestMethod.DELETE))
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ResponseBody
  def deletePoll(@PathVariable mod_id:String , @PathVariable poll_id:String): Any = {
    
    val mit = modList.iterator()
    while (mit.hasNext()) {
      val mod: Moderator = mit.next()
      if ((mod.mod_id.toString()).equals(id)) {
        
        val pit = mod.pList.iterator()
        while(pit.hasNext){
          val p: Poll = pit.next()
          mod.pList.remove(poll)
            return "Deleted successfully"
        }
      }
    }
  }
  
  @RequestMapping(value=Array("/polls/{poll_id}") ,method=Array(RequestMethod.PUT))
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ResponseBody
  def pollVote(@PathVariable poll_id:String, @RequestParam("choice") choice_index:Int): Any = {
    
    val mit = modList.iterator()
    while (mit.hasNext()) {
      val mod: Moderator = mit.next()
      val pit = mod.pList.iterator()
      while(pit.hasNext){
        val p : Poll = pit.next()
        
        if((p.poll_id.toString()).equals(poll_id)){
          
          val results : ArrayList[Int] = p.getResults()
         
            val res : Int = results.get(choice_index)
            
            results.set(choice_index, res+1)
           }
            return 
         }
      }
        
    }
    
  def allowModLogin(auth: String): Boolean = {
    if(auth==null){
      return false
    }
    if(!auth.toUpperCase().startsWith("BASIC")){
      return false
    }
    var encodedModPassword: String = auth.substring(6);
    
    var decoder: sun.misc.BASE64Decoder = new sun.misc.BASE64Decoder()
    var decodedModPassword: String = new String(decoder.decodeBuffer(encodedModPassword))
    if ("authorized".equals(checkUser.get(decodedModPassword))) {
       return true;
    } else {
       return false;
    }
  }
  
  def doHttpBasicAuth(req: HttpServletRequest, res: HttpServletResponse) = {
    
        var auth: String = req.getHeader("Authorization") 
        if(!allowModLogin(auth)){
          res.setHeader("WWW-Authenticate", "BASIC realm=\"Provide credentials\"")
          res.sendError(HttpServletResponse.SC_UNAUTHORIZED)
        }else{
          System.out.println("Login successfull")
        }
    }
  def validate(bindingResult: BindingResult, msg : String): String = {
    var errMsg: String=""
    errMsg+=msg
    if(bindingResult.hasErrors()){
       for(error <- bindingResult.getFieldErrors().asScala){
         errMsg+=error.getField + " - " + "is required" + "\n"
       }
      return errMsg.toString()
    }else{
      ""
    }
  }
  
}